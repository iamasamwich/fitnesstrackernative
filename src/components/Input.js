// a form input field
// pass heading (text), valid (bool), value (text), placeholder(text), keyboard (text), secure (bool), and handleTextChange (callback)

{/* <Input 
        heading="Confirm Password"
        valid={confPasswordOk}
        value={confPword}
        placeholder="Confirm Password"
        keyboard="default"
        secure={true}
        handleTextChange={setConfPword}
      /> */}



import React from 'react';
import { StyleSheet, Text, TextInput, View } from 'react-native';

const Input = ({heading, valid, value, placeholder, keyboard, secure, handleTextChange}) => {

  return (
    <View style={styles.segment}>
      <Text style={styles.heading}>{heading}</Text>
      <TextInput
        style={[styles.input, valid ? styles.valid : styles.invalid]}
        value={value}
        placeholder={placeholder}
        onChangeText={handleTextChange}
        keyboardType={keyboard}
        secureTextEntry={secure}
      />
    </View>
  )
};

const styles = StyleSheet.create({
  heading: {
    fontWeight: 'bold',
    fontSize: 20
  },
  input: {
    width: '100%',
    fontSize: 22,
    height: 35,
    paddingLeft: 10
  },
  invalid: {
    backgroundColor: '#ffeeee',
    borderColor: 'red',
    borderWidth: 1
  },
  segment: {
    width: '100%',
    paddingBottom: 10
  },
  valid: {
    backgroundColor: '#fff'
  },
})

export default Input;
