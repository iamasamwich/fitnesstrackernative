import React from 'react';

import {View, Text, TouchableOpacity, StyleSheet, StatusBar} from 'react-native';
import { connect } from 'react-redux';
import Loading from './status/Loading';
import DefaultStatus from './status/DefaultStatus';

const Status = ({appStatus, page}) => {

  
  const showStatus = () => {
    switch (appStatus) {
      case 'loading':
        return <Loading />
      default: 
        return <DefaultStatus status={appStatus} />
    };
  };

  return (
    <View style={styles.main}>
      {showStatus()}
    </View>

  )
};

const styles = StyleSheet.create({
  main: {
    position: 'absolute',
    flex: 1,
    height: '100%',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    top: StatusBar.currentHeight + 50,
    opacity: 1,
    backgroundColor: '#beeeef',
  }
})

const mapStateToProps = ({appStatus, page}) => {
  return {
    appStatus,
    page
  };
};

export default connect(mapStateToProps)(Status);