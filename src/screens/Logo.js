import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

const Logo = () => {
  return (
    <View style={styles.logo}>
      <Text>Cycle Tracker</Text>
      <View style={styles.wheel} id="logo-wheel">
        <View style={styles.spokes, styles.spokes1} />
        <View style={styles.spokes, styles.spokes2} />
        <View style={styles.spokes, styles.spokes3} />
        <View style={styles.spokes, styles.spokes4} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    position: 'relative',
    width: 300,
    height: 370,
  },
  wheel: {
    position: 'absolute',
    top: '58%',
    left: '50%',
    backgroundColor: 'green'
  },
  spokes: {

  },
  spokes1: {

    
  },
  spokes2: {

  },
  spokes3: {

  },
  spokes4: {

  }
})

export default Logo;