import React, { useState, useEffect } from 'react';
import { Text, StyleSheet, StatusBar, View, TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';
import { signUp, changePage } from '../actions';

import Input from '../components/Input';

const SignUp = ({signUp, changePage}) => {

  const [stage, setStage] = useState(0);

  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [pword, setPword] = useState('');
  const [confPword, setConfPword] = useState('');
  const [tandc, setTandc] = useState(false);

  const [nameOk, setnameOk] = useState(true);
  const [emailOk, setEmailOk] = useState(true);
  const [passwordOk, setPasswordOk] = useState(true);
  const [confPasswordOk, setConfPasswordOk] = useState(true);

  const [anyError, setAnyError] = useState('');

  useEffect(() => {
    if (name) {
      setnameOk(true);
    } else {
      setnameOk(false);
    };
  }, [name]);

  useEffect(() => {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (re.test(email.toLowerCase())) {
      setEmailOk(true);
    } else {
      setEmailOk(false);
    };
  }, [email]);

  useEffect(() => {
    if (pword && pword === confPword) {
      setPasswordOk(true);
      setConfPasswordOk(true);
    } else if (pword && pword !== confPword) {
      setPasswordOk(true);
      setConfPasswordOk(false);
    } else if (!pword) {
      setPasswordOk(false);
      setConfPasswordOk(false);
      setConfPword('');
    };
  }, [pword, confPword]);

  useEffect(() => {
    if (emailOk && passwordOk && confPasswordOk && tandc) {
      setAnyError(false);
    } else {
      setAnyError(true);
    };
  }, [nameOk, emailOk, passwordOk, confPasswordOk, tandc]);

  const showConfPassword = () => {
    return passwordOk 
    ?
      <Input 
        heading="Confirm Password"
        valid={confPasswordOk}
        value={confPword}
        placeholder="Confirm Password"
        keyboard="default"
        secure={true}
        handleTextChange={setConfPword}
      />
    : null;
  };

  const showTandC = () => {
    return (nameOk && emailOk && passwordOk && confPasswordOk) 
    ?
      <View style={[styles.segment, styles.inline]}>
        <TouchableOpacity 
        style={[styles.checkBox, tandc ? {backgroundColor: 'green'} : styles.invalid]}
        onPress={() => setTandc(!tandc)}
         />
        <Text>I have read and agree to the terms and conditions</Text>
      </View>
    : null;
  };

  const showButtons = () => {
    const showNext = () => {
      if (stage === 0 && emailOk && nameOk) {
        return (
          <TouchableOpacity 
            style={[styles.button, styles.buttonNav]}
            onPress={() => setStage(1)}>
            <Text style={styles.heading}>Next</Text>
          </TouchableOpacity>
        );
      };
    };

    const showBack = () => {
      if (stage === 1) {
        return (
          <TouchableOpacity 
            style={[styles.button, styles.buttonNav]}
            onPress={() => setStage(0)}>
            <Text style={styles.heading}>Back</Text>
          </TouchableOpacity>
        );
      };
    };

    const showCancel = () => {
      return (
        <TouchableOpacity 
          style={[styles.button, styles.cancel]}
          onPress={() => changePage('login')}>
          <Text style={styles.heading}>Cancel</Text>
        </TouchableOpacity>
      );
    };

    const showSubmit = () => {
      if (!anyError && stage === 1) {
        return (
          <TouchableOpacity 
            style={styles.button}
            onPress={() => handleSubmit()}>
            <Text style={styles.heading}>
              Sign Up!
            </Text>
          </TouchableOpacity>
        );
      };
    };

    return (
      <View style={styles.buttonContainer}>
        {showSubmit()}
        {showNext()}
        {showBack()}
        {showCancel()}
      </View>
    );
  };

  const handleSubmit = () => {
    if (anyError) {
      return;
    };
    signUp({name, email, pword});
  };

  return (
    <View style={styles.main}>
      <Text style={styles.h1}>Create An Account</Text>
        <View style={styles.form}>
          {stage === 0 
            ?
            <>
              <Input
                heading="What should we call you?"
                valid={nameOk}
                value={name}
                placeholder="Freddles"
                keyboard="default"
                secure={false}
                handleTextChange={setName}
              />

              <Input 
                heading="Email"
                valid={emailOk}
                value={email}
                placeholder="winifred@email.com"
                keyboard="email-address"
                secure={false}
                handleTextChange={setEmail}
              />
            </>
            : null
          }

          {stage === 1 
            ?
            <>
              <Input 
                heading="Set Password"
                valid={passwordOk}
                value={pword}
                placeholder="Password"
                keyboard="default"
                secure={true}
                handleTextChange={setPword}
              />

              {passwordOk ?
                <Input 
                  heading="Confirm Password"
                  valid={confPasswordOk}
                  value={confPword}
                  placeholder="Confirm Password"
                  keyboard="default"
                  secure={true}
                  handleTextChange={setConfPword}
                />
                : null
              }
            {showTandC()}

            </>
            : null
          }
          {showButtons()}
        </View>
    </View>
  );
};

const styles = StyleSheet.create({
  button: {
    backgroundColor: 'springgreen',
    height: 35,
    flex: 1,
    marginLeft: 5,
    marginRight: 5,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
  },
  buttonNav: {
    backgroundColor: 'cyan'
  },
  cancel: {
    backgroundColor: 'tomato'
  },
  checkBox: {
    height: 15,
    width: 15,
    borderRadius:7.5,
    borderWidth: 1,
    backgroundColor: 'white',
    margin: 5,
    marginRight: 15
  },
  form: {
    flex: 5,
    width: '80%',
    height: 200,
  },
  h1: {
    fontSize: 30,
    fontWeight: 'bold',
    padding: 15
  },
  heading: {
    fontWeight: 'bold',
    fontSize: 20
  },
  inline: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  invalid: {
    backgroundColor: '#ffeeee',
    borderColor: 'red',
    borderWidth: 1
  },
  main: {
    flex: 1,
    height: '100%',
    width: '100%',
    paddingTop: StatusBar.currentHeight + 50,
    alignItems: 'center',
  },
  segment: {
    width: '100%',
    paddingBottom: 10
  },
});

export default connect(null, {signUp, changePage})(SignUp);