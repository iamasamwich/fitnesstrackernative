import React, {useState, useEffect} from 'react';
import { connect } from 'react-redux';
import { changePage, createSession } from '../actions';
import DateTimePicker from '@react-native-community/datetimepicker';


import { StatusBar, StyleSheet, Text, View, Button, TextInput, TouchableOpacity} from 'react-native';

import Input from '../components/Input';

const CreateSession = ({changePage, createSession}) => {

  const twoDigits = num => {
    return num < 10 ?
      "0" + num :
      num;
  };
  const d = new Date();

  const [stage, setStage] = useState(0);

  const [date, setDate] = useState(d.getDate() + '-' + twoDigits((d.getMonth() + 1)) + '-' + d.getFullYear());
  const [distance, setDistance] = useState('');
  const [secs, setSecs] = useState('');
  const [mins, setMins] = useState('');
  const [hours, setHours] = useState('');
  const [weight, setWeight] = useState('');
  const [route, setRoute] = useState('');

  const [dateOk, setDateOk] = useState(true);
  const [distanceOk, setDistanceOk] = useState(true);
  const [timeOk, setTimeOk] = useState(true);
  const [weightOk, setWeightOk] = useState(true);
  const [routeOk, setRouteOk] = useState(true);

  const [anyError, setAnyError] = useState(false);

  const [show, setShow] = useState(false);

  useEffect(() => {
    const re = /^\d{2}-\d{2}-\d{4}$/;
    if (re.test(date)) {
      setDateOk(true);
    } else {
      setDateOk(false);
    };
  }, [date]);

  useEffect(() => {
    const re = /^[0-9]{0,3}[.]{0,1}[0-9]{0,3}$/;
    if (!isNaN(distance) && distance > 0 && re.test(distance) && Number(distance) < 1000) {
      setDistanceOk(true);
    } else {
      setDistanceOk(false);
    };
  }, [distance]);

  useEffect(() => {

    if (
      (!isNaN(hours) || !isNaN(mins) || !isnan(secs)) &&
      (Number(hours) >= 0 || Number(mins) >= 0 || Number(secs) >= 0) &&
      ((Number(hours) + Number(mins) + Number(secs)) > 0) &&
      (Number(hours) % 1 === 0) &&
      (Number(mins) % 1 === 0) &&
      (Number(secs) % 1 === 0)
    ) {

      if (Number(mins) > 59) {
        const newMins = Number(mins) % 60;
        const newHours = (Number(mins) - newMins) / 60;
        setHours((Number(hours) + newHours).toString());
        setMins(newMins.toString());
      };

      if (Number(secs) > 59) {
        const newSecs = Number(secs) % 60;
        const newMins = (Number(secs) - newSecs) / 60;
        setMins((Number(mins) + newMins).toString());
        setSecs(newSecs.toString());
      };
      setTimeOk(true);
    } else {
      setTimeOk(false);
    };

  }, [hours, mins, secs]);

  useEffect(() => {
    if (!isNaN(weight) && weight) {
      setWeightOk(true);
    } else {
      setWeightOk(false);
    };
  }, [weight]);

  useEffect(() => {
    if (route) {
      setRouteOk(true);
    } else {
      setRouteOk(false);
    };
  }, [route]);

  useEffect(() => {
    if (dateOk && distanceOk && timeOk && weightOk && routeOk){
      setAnyError(false);
    } else {
      setAnyError(true);
    };
  }, [dateOk, distanceOk, timeOk, weightOk, routeOk]);

  const handleDateChange = (event, dateInput) => {
    if (dateInput === undefined) {
      setShow(false);
      return;
    } else {
      const newDate = dateInput.getDate() + '-' + twoDigits((dateInput.getMonth() + 1)) + '-' + dateInput.getFullYear()
      setShow(false);
      setDate(newDate);
    };
  };

  


  const showButtons = () => {

    const showNext = () => {
      if (stage === 0 && dateOk && distanceOk && timeOk) {
        return (
          <TouchableOpacity 
            style={[styles.button, styles.buttonNav]}
            onPress={() => setStage(1)}>
            <Text style={styles.heading}>Next</Text>
          </TouchableOpacity>
        );
      };
    };

    const showBack = () => {
      if (stage === 1) {
        return (
          <TouchableOpacity 
            style={[styles.button, styles.buttonNav]}
            onPress={() => setStage(0)}>
            <Text style={styles.heading}>Back</Text>
          </TouchableOpacity>
        );
      };
    };

    const showSubmit = () => {
      if (!anyError) {
        return (
          <TouchableOpacity 
            style={[styles.button]}
            onPress={() => formSubmit()}>
            <Text style={styles.heading}>Add It!</Text>
          </TouchableOpacity>
        );
      };
    };

    const showCancel = () => {
      return (
        <TouchableOpacity 
           style={[styles.button, styles.cancel]}
           onPress={() => changePage('login')}>
           <Text style={styles.heading}>Cancel</Text>
         </TouchableOpacity>
      );
    };

    return (
      <View style={styles.buttonContainer}>
        {showSubmit()}
        {showNext()}
        {showBack()}
        {showCancel()}
      </View>
    );
  };

  const formSubmit = () => {
    if (anyError) {
      return;
    } else {
      createSession({date, distance, hours, mins, secs, weight, route});
    };
  };

  return (
    <View style={styles.main}>
      <Text style={styles.h1}>Create A Session</Text>
      <View style={styles.form}>
        {stage === 0
          ?
          <>
            <View style={styles.segment}>
              <Text style={styles.heading}>Date</Text>
              <TouchableOpacity 
                style={[styles.input, styles.valid]}
                onPress={() => setShow(true)}>
                <Text style={styles.inputText}>{date.toString()}</Text>
              </TouchableOpacity>
            </View>
            {show ?
              <DateTimePicker
                value={d}
                mode="date"
                display="spinner"
                onChange={handleDateChange}
              />
              : null
            }
            <Input 
              heading="Distance"
              valid={distanceOk}
              value={distance}
              placeholder="km.mmm"
              keyboard="numeric"
              secure={false}
              handleTextChange={setDistance}
            />
            <View style={styles.segment}>
              <Text style={styles.heading}>Session Time</Text>
              <View style={styles.inline}>
                <TextInput 
                  style={[styles.input, styles.timeInput, timeOk ? styles.valid : styles.invalid]}
                  value={hours}
                  keyboardType="numeric"
                  onChangeText={setHours}
                  placeholder="hh"
                />
                <TextInput 
                  style={[styles.input, styles.timeInput, timeOk ? styles.valid : styles.invalid]}
                  value={mins}
                  keyboardType="numeric"
                  onChangeText={setMins}
                  placeholder="mm"
                />
                <TextInput 
                  style={[styles.input, styles.timeInput, timeOk ? styles.valid : styles.invalid]}
                  value={secs}
                  keyboardType="numeric"
                  onChangeText={setSecs}
                  placeholder="ss"
                />
              </View>
            </View>
          </>
          : 
          <>
            <Input 
              heading="Weight"
              valid={weightOk}
              value={weight}
              placeholder="kg"
              keyboard="numeric"
              secure={false}
              handleTextChange={setWeight}
            />

            <Input 
              heading="Route"
              valid={routeOk}
              value={route}
              placeholder="To the park"
              keyboard="default"
              secure={false}
              handleTextChange={setRoute}
            />
          </>
        }
        {showButtons()}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  button: {
    backgroundColor: 'springgreen',
    height: 35,
    flex: 1,
    marginLeft: 5,
    marginRight: 5,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
  },
  buttonNav: {
    backgroundColor: 'cyan'
  },
  cancel: {
    backgroundColor: 'tomato'
  },
  form: {
    flex: 5,
    width: '80%',
  },
  h1: {
    fontSize: 30,
    fontWeight: 'bold',
    padding: 15
  },
  heading: {
    fontWeight: 'bold',
    fontSize: 20
  },
  inline: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  input: {
    width: '100%',
    fontSize: 22,
    height: 35,
    paddingLeft: 10
  },
  inputText: {
    fontSize: 22,
    color: 'black'
  },
  invalid: {
    backgroundColor: '#ffeeee',
    borderColor: 'red',
    borderWidth: 1
  },
  main: {
    flex: 1,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: StatusBar.currentHeight + 50,
  },
  segment: {
    width: '100%',
    paddingBottom: 10
  },
  timeInput: {
    width: '30%'
  },
  valid: {
    backgroundColor: '#fff'
  },
})

export default connect(null, {changePage, createSession})(CreateSession);
