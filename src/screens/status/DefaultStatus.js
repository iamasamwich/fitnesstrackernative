import React from 'react';
import {connect} from 'react-redux';
import {TouchableOpacity, View, Text, StyleSheet} from 'react-native';
import {clearError, changePage} from '../../actions';

const DefaultStatus = ({clearError, changePage, status}) => {

  const showError = () => {
    switch (status) {
      case 401:
        return 'Login Details Incorrect';
      case 406:
        return 'Unacceptable Inputs';
      case 409:
        return 'Duplication';
      case 500:
        return 'It looks like the server caught fire!';
      default:
        return 'Non-specific errors';
    };
  };

  const containerClicked = e => {
    changePage('home');
    clearError();
  };

  return (
    <TouchableOpacity
      onPress={containerClicked}>
      <Text style={[styles.statusBox, styles.h1]}>Womp womp!</Text>
      <Text style={[styles.statusBox, styles.h1]}>{showError()}</Text>
      <Text style={[styles.statusBox, styles.h3]}>**Touch anywhere to dismiss**</Text>

    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  h1: {
    fontSize: 30,
    fontWeight: 'bold'
  },
  h3: {
    fontSize: 20
  },
  statusBox: {
    textAlign: 'center',
    padding: 20,
  }
})

export default connect(null, {clearError, changePage})(DefaultStatus);