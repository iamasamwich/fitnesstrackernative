import React from 'react';
import { Text, View, StyleSheet, StatusBar, Image} from 'react-native';

const Loading = () => {
  return (
      <>
        <Image
          style={styles.logo}
          source={require('../../../assets/ft-logo-trans.png')} />
        <Text style={styles.h1}>Loading...</Text>
      </>
  )
};

const styles = StyleSheet.create({
  h1: {
    fontSize: 40,
    fontWeight: 'bold'
  },
  logo: {
    height: 300,
    width: 300
  },
});

export default Loading;