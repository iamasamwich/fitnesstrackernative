import React, {useEffect, Fragment} from 'react';
import { connect } from 'react-redux';
import { StyleSheet, StatusBar, View, Text, SafeAreaView} from 'react-native';

import {ping, changePage, logout} from '../actions';

import Login from './Login';
import Choice from './Choice';
import Status from './Status';
import NavBar from './NavBar';
import SignUp from './SignUp';
import CreateSession from './CreateSession';
import ViewData from './ViewData';

const Root = ({ login, page, appStatus, ping}) => {

  useEffect(() => {
    ping();
  }, []);

  console.log('Root page?', page);
  console.log('root status', appStatus);

  const logged = () => {
    switch (page) {
      case 'home':
        return <Choice />;
      case 'createSession':
        return <CreateSession />
      case 'viewData':
        return <ViewData />
      default: 
        return <Choice />;
    };
  };

  const unlogged = () => {
    switch (page) {
      case 'signup': 
        return <SignUp />
      default: 
        return <Login />;
    }
  }

  return (
    <SafeAreaView style={styles.main}>
      <NavBar />
      {appStatus !== null ? <Status /> : login ? logged() : unlogged()}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  main: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#beeeef',
    paddingTop: StatusBar.currentHeight
  }
});

const mapStateToProps = ({login, page, appStatus}) => {
  return {
    login,
    page,
    appStatus
  };
};

export default connect(mapStateToProps, {
  ping,
  changePage,
  logout
})(Root);