import React, {Fragment, useState} from 'react';
import {View, Text, StyleSheet, TouchableOpacity, StatusBar, Pressable} from 'react-native';
import { connect } from 'react-redux';

import {logout, changePage} from '../actions';

const NavBar = ({login, appStatus, logout, changePage, page}) => {

  const [menu, setMenu] = useState(false);

  const handleHomeClick = () => {
    if (menu) {
      setMenu(false);
    };
    changePage('login');
  };

  const showHomeButton = () => {
      if (page === 'login' || page === 'choice') {
        return;
      } else {
        return (
          <TouchableOpacity 
            style={styles.buttonHouse} id='home'
            onPress={handleHomeClick}>
            <Text style={styles.icon}>{"\u2302"}</Text>
          </TouchableOpacity>
        )
      }
  };

  const menuClicked = (id) => {
    menu ? setMenu(false) : null;
    if (id === 'logout') {
      logout();
    } else if (id === 'about') {
      changePage('about');
    };

  }

  const showMenu = () => {
    return menu ?
      <TouchableOpacity
        onPress={() => menu === true ? setMenu(false) : null}
        style={styles.modal}>
        <View style={styles.menu}>
          {login ?
          <TouchableOpacity 
            style={styles.menuItemContainer}
            onPress={() => menuClicked('logout')}
            >
            <Text   
              style={styles.menuItem}
              >Log Out</Text>
          </TouchableOpacity>
          : 
          null}
          <TouchableOpacity 
            style={styles.menuItemContainer}
            onPress={() => menuClicked('about')}>
            <Text style={styles.menuItem}>About</Text>
          </TouchableOpacity>
        </View>
      </TouchableOpacity>

      :
      null
  };

  const clickBurger = () => {
    setMenu(!menu);
  };

  return (
    <Fragment>
      <Pressable 
        style={styles.navBar}
        onPress={() => menu ? setMenu(false) : null}>
        <View style={styles.menuIcons}>
          {showHomeButton()}
          <TouchableOpacity 
            style={styles.buttonHouse}
            id='burger'
            onPress={clickBurger}>
            <Text style={styles.icon}>{"\u2630"}</Text>
          </TouchableOpacity>
        </View>
      </Pressable>
      {showMenu()}
    </Fragment>
  )
};

const styles = StyleSheet.create({
  buttonHouse: {
    height: 50,
    width: 50
  },
  icon: {
    fontSize: 35,
    textAlign: 'center'
  },
  menu: {
    right: 0,
    width: '60%',
    alignItems: 'flex-end',
    borderRadius: 3,
    padding: 10,
    zIndex: 500
  },
  menuIcons: {
    height: 50,
    flexDirection: 'row',
  },
  menuItem: {
    fontSize: 25,
    fontWeight: '300',
    textAlign: 'right'
  },
  menuItemContainer: {
    width: '100%',
    padding: 10,
    backgroundColor: '#fff'
  },
  modal: {
    position: 'absolute',
    flex: 1,
    alignItems: 'flex-end',
    top: StatusBar.currentHeight + 50,
    opacity: .8,
    width: '100%',
    height: '100%',
    backgroundColor: '#fff',
    zIndex: 10000
  },
  navBar: {
    position: 'absolute',
    top: 0,
    minHeight: StatusBar.currentHeight + 50,
    flexDirection: 'column',
    alignItems: 'flex-end',
    width: '100%',
    paddingRight: 10,
    paddingTop: StatusBar.currentHeight,
    zIndex: 500,
  }
});

const mapStateToProps = ({login, page, appStatus}) => {
  return {
    login, 
    page, 
    appStatus
  };
};


export default connect(mapStateToProps, {logout, changePage})(NavBar);