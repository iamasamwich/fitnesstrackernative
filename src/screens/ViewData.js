import React, {useState, useEffect} from 'react';
import { connect } from 'react-redux';

import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';

import {getMonthSessions, getAllSessions} from '../actions';

import Loading from './status/Loading';
import Graph from './Graph';

const ViewData = ({sessions, getMonthSessions, getAllSessions, fetchAll}) => {

  const [fetchedSessions, setFetchedSessions] = useState();
  const [display, setDisplay] = useState('speed');
  const [period, setPeriod] = useState('month');
  const [status, setStatus] = useState(null);

  useEffect(() => {
    if (status === 'loading') {
      return;
    };
    setFetchedSessions();
    setStatus('loading');
    if (period === 'month') {
      getMonthSessions();
    } else {
      getAllSessions();
    };
  }, [period]);

  useEffect(() => {
    setStatus(null);
    setFetchedSessions(sessions);
  }, [sessions]);

  useEffect(() => {
    if (fetchAll === true) {
      setPeriod('all');
    };
  }, [fetchAll]);

  const showButtons = () => {
    const setColour = (disp) => {
      return disp === display ? "green" : "blue";
    };

    const showSpeed = () => {
      return (
        <TouchableOpacity
          style={[styles.button, styles[setColour('speed')]]}
          onPress={() => setDisplay('speed')}>
          <Text style={styles.heading}>Speed</Text>
        </TouchableOpacity>
      );
    };

    const showTime = () => {
      return (
        <TouchableOpacity
          style={[styles.button, styles[setColour('time')]]}
          onPress={() => setDisplay('time')}>
          <Text style={styles.heading}>Time</Text>
        </TouchableOpacity>
      );
    };

    const showWeight = () => {
      return (
        <TouchableOpacity
          style={[styles.button, styles[setColour('weight')]]}
          onPress={() => setDisplay('weight')}>
          <Text style={styles.heading}>Weight</Text>
        </TouchableOpacity>
      );
    };

    return (
      <View style={styles.buttonContainer}>
        {showSpeed()}
        {showTime()}
        {showWeight()}
      </View>
    );
  }

  const showPeriodSelect = () => {
    return (
      <View style={[styles.buttonContainer, styles.paddingBottom]}>
        <TouchableOpacity 
          style={[styles.button, styles.green]}
          onPress={() => setPeriod(period === 'month' ? 'all' : 'month')}>
          <Text style={styles.heading}>
            {period === 'month'
              ?
              'Show All Sessions'
              :
              'Show Month Sessions'
            }
          </Text>
        </TouchableOpacity>
      </View>
    )
  };

  return (
    <View style={styles.main}>
      {!fetchedSessions || fetchedSessions.length === 0
        ?
         <Loading />
        :
          <>
            <Graph display={display} fetchedSessions={fetchedSessions} />
            <View 
              style={{bottom: 0, height: 120, width: '100%'}}
            >
              {showButtons()}
              {showPeriodSelect()}
            </View>
          </>
      }
    </View>
  )
};

const styles = StyleSheet.create({
  blue: {
    backgroundColor: 'cyan'
  },
  button: {
    height: 35,
    flex: 1,
    marginLeft: 5,
    marginRight: 5,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    paddingTop: 15,
  },
  graphContainer: {
    flex: 1,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'gainsboro'
  },
  green: {
    backgroundColor: 'springgreen'
  },
  heading: {
    fontWeight: 'bold',
    fontSize: 20
  },
  main: {
    flex: 1,
    width: "100%",
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 50,
    backgroundColor: 'pink'
  },
  paddingBottom: {
    paddingBottom: 20,
    backgroundColor: 'orange'
  }
});


const mapStateToProps = ({sessions, fetchAll}) => {
  return {
    sessions, fetchAll
  };
};

export default connect(mapStateToProps, {getMonthSessions, getAllSessions})(ViewData);