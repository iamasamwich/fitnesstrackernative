import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { VictoryLine, VictoryBar, VictoryChart, VictoryTheme } from "victory-native";

const data = [
  { quarter: 1, earnings: 13000 },
  { quarter: 2, earnings: 16500 },
  { quarter: 3, earnings: 14250 },
  { quarter: 4, earnings: 19000 }
];


const Graph = ({display, fetchedSessions}) => {

  const [graphSize, setGraphSize] = useState({height: 0, width: 0})

  useEffect(() => {

  }, [graphSize]);

  const getSize = e => {
    const {x , y, height, width} = e.nativeEvent.layout;
    setGraphSize({height: height, width: width});
  };


  const graphData = () => {
    return fetchedSessions.map(session => {
      return {
        x: session.date,
        y: session.distance
      }
    });
  };

  const distances = fetchedSessions.map(session => {
    return session.distance;
  });
  const distanceMax = Math.max(...distances);
  const distanceMin = Math.min(...distances);


  return (
    <View 
      style={[styles.fill, styles.center]}
      onLayout={getSize}
    >
      <View style={styles.container}>
        <VictoryChart 
          height={graphSize.height}
        >
        <VictoryLine 
          style={{data: {stroke: '#f00'}}}
          data={graphData()}
        >



        </VictoryLine>
        </VictoryChart>
      </View>

    </View>
  )

};

const styles = StyleSheet.create({
  fill: {
    flex: 1,
    width: '100%',
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  container: {
    flex: 1,
    width: '100%',
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "lightblue"
  },
})

export default Graph;