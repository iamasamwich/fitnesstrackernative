import React, {useState, useEffect} from 'react';
import {View, Text, StyleSheet, TextInput, Button, Image, StatusBar, TouchableHighlight} from 'react-native';
import { connect } from 'react-redux';
import {login, changePage} from '../actions';

import Input from '../components/Input';

const Login = ({login, changePage}) => {

  const [email, setEmail] = useState('sam@sam.com');
  const [pword, setPword] = useState('password');

  const [emailOk, setEmailOk] = useState(true);
  const [passwordOk, setPasswordOk] = useState(true);

  const [anyError, setAnyError] = useState(false);

  useEffect(() => {
    const re = /^(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/;
    const emailCheck = re.test(email.toLowerCase());

    if (emailCheck) {
      setEmailOk(true);
    } else {
      setEmailOk(false);
    };
  }, [email]);

  useEffect(() => {
    if (pword) {
      setPasswordOk(true);
    } else {
      setPasswordOk(false);
    };
  }, [pword]);

  useEffect(() => {
    if (emailOk && passwordOk) {
      setAnyError(false);
    } else {
      setAnyError(true);
    };
  }, [emailOk, passwordOk]);

  const buttonClick = (e) => {
    if (!anyError) {
      login({email, pword});
    } else {
      return;
    };
  };

  return (
    <View style={styles.main}>
      <View style={styles.logo}>
        <Image 
          style={styles.logoImage}
          resizeMode='contain'
          source={require('../../assets/ft-logo-trans.png')} />
        <Text style={styles.h1}>Cycle Tracker</Text>
      </View>

      <View style={styles.form}>
        
        <Input 
          heading="Email:"
          valid={emailOk}
          value={email}
          placeholder="Email"
          keyboard="email-address"
          secure={false}
          handleTextChange={setEmail}
        />

        <Input 
          heading="Password:"
          valid={passwordOk}
          value={pword}
          placeholder="Password"
          keyboard="default"
          secure={true}
          handleTextChange={setPword}
        />

        {anyError ?
          null
          :
          <TouchableHighlight
            style={styles.button}
            title='Log In'
            onPress={buttonClick}>
            <Text style={styles.heading}>
              Log In
            </Text>
          </TouchableHighlight>
        }

        <Text 
          style={styles.fakeLink}
          onPress={() => changePage('signup')}>Create an account</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  main: {
    flex: 1,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: StatusBar.currentHeight,
  },
  logo: {
    width: '100%',
    flex: 3,
    justifyContent: 'center',
    alignItems: 'center',
  },
  logoImage: {
    width: '60%',
    height: '60%',
  },
  h1: {
    fontSize: 40,
    fontWeight: 'bold'
  },
  form: {
    flex: 5,
    width: '80%',
    height: 200,
  },
  heading: {
    fontWeight: 'bold',
    fontSize: 20
  },
  button: {
    backgroundColor: 'springgreen',
    height: 35,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10
  },
  fakeLink: {
    color: 'blue',
    textDecorationLine: 'underline',
    textAlign: 'center',
    marginTop: 15
  },
  error: {
    backgroundColor: 'tomato'
  }
});

export default connect(null, {
  login,
  changePage
})(Login);