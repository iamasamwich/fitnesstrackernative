import React from 'react';
import {connect} from 'react-redux';
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native';
import {changePage} from '../actions';

const Choice = ({changePage}) => {
  return (
    <View style={styles.main}>
      <View style={styles.buttonBox}>
        <TouchableOpacity 
          onPress={() => changePage('createSession')}
          style={styles.choiceButton}>
          <Text style={styles.buttonText}>CREATE SESSION</Text>
        </TouchableOpacity>
        <TouchableOpacity 
          onPress={() => changePage('viewData')}
          style={styles.choiceButton}>
          <Text style={styles.buttonText}>VIEW SESSION DATA</Text>
        </TouchableOpacity>

      </View>
    </View>
  )
};

const styles = StyleSheet.create({
  buttonBox: {
    width: '60%',
    height: '50%',
    justifyContent: 'space-between'
  },
  buttonText: {
    fontWeight: 'bold',
    fontSize: 20
  },
  choiceButton: {
    height: '40%',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    borderWidth: 1,
    backgroundColor: '#fff',
  },
  main: {
    flex: 1,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  }
})

export default connect(null, {changePage})(Choice);