const host = () => {
  if (process.env.NODE_ENV === 'development') {
    return "http://192.168.0.3:3000"
  }
}

const api = {
  url: `${host()}`,
  options: {
    headers: {'Content-Type': 'application/json'},
    credentials: 'include'
  }
};

export default api;